======================================================================================
									Mega Man Rock Force
======================================================================================

The original fangame was a creation by GoldWaterDLS. Check out the official site!
http://megamanrockforce.com/rockforcemain.html
And the credits for the original fangame
http://megamanrockforce.com/rockforcecredits.html

X=============X
| Map Credits |
X=============X

Crypt Man - Jake
Photon Man - LlamaHombre
Pulse Man - Lego / Russel
Virus Man - Maxine / Yellow Devil
Fuse Man - Maxine / Yellow Devil
Shock Man - Maxine / Yellow Devil
Circuit Man - Mendez / Bluebrawl
Charade Man - Mendez / Bluebrawl

Terror Man -  Jake
Flare Man - Mendez / Bluebrawl
Plague Man - Maxine / Yellow Devil
Power Man - Dr. Freeman (+Watzup)

Thrill Man - Lego / Russel
Thermo Man - Jake
War Man - Mendez / Bluebrawl
Port Man - Dr. Freeman

Death Man - LlamaHombre
Fish Man -  Mendez / Bluebrawl
Polar Man - Mendez / Bluebrawl
Justice Man - Maxine / Yellow Devil

General V2 Updating - LlamaHombre, Lego / Russel, Mendez / BlueBrawl, Trillster

General V3 Updating - Trillster, Lego / Russel, Pegg

X===============X
| Misc  Credits |
X===============X

Props not listed here were made by Maxine / Yellow Devil

Crypt cloak unused 3D models - Yoshiatom
Block snake unused 3D models - from Classes
Thrill Wedge voxel - Mendez / Bluebrawl
Various other voxels - Trillster

Crypt Man Cerberus rotations - FTX6004
Virus Man blob enemy rotations - FTX6004
Power Man Sparky enemies - Blaze Yeager
War Man stomper robots - Pegg
Flare Man flare extra frame - Lego / Russel
Flare Man gorilla - FTX6004
Polar Man igloo - FTX6004

Polar Man and Fish Man mugshots - Pegg
Extra Justiceman rotations - Blaze Yeager

Lego - ACS/Decorate assistance/overhaul/addition
Trillster - campaign cutscenes and boss coding

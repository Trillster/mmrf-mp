///// MMRFPUL /////
texture PULPULX
    pic PULPULA tics 2
	pic PULPULB tics 2
	pic PULPULC tics 2
	pic PULPULD tics 2
	
texture PULPULNX
    pic PULPULNA tics 2
	pic PULPULNB tics 2
	pic PULPULNC tics 2
	pic PULPULND tics 2
	
texture PULPUUA range PULPUUD tics 2

////////////////////
///// MMRFSHO /////
flat SHOWATX
pic SHOWAT01 tics 12
pic SHOWAT02 tics 12
pic SHOWAT03 tics 12
pic SHOWAT04 tics 12

texture SHOFALX1
    allowdecals
    pic SHOFAL01 tics 3
	pic SHOFAL02 tics 3
	pic SHOFAL03 tics 3
	
	texture SHOFALX2
    allowdecals
    pic SHOFAL04 tics 3
	pic SHOFAL05 tics 3
	pic SHOFAL06 tics 3

////////////////////
///// MMRFCIR /////
texture CIRCONV1
    allowdecals
    pic CIRCONVA tics 3
    pic CIRCONVB tics 3
	pic CIRCONVA tics 3
    pic CIRCONVB tics 3

texture CIRCONV2
    allowdecals
    pic CIRCONVC tics 3
    pic CIRCONVD tics 3
	pic CIRCONVC tics 3
    pic CIRCONVD tics 3
	
texture CIRCONV3
    allowdecals
    pic CIRCONVE tics 3
    pic CIRCONVF tics 3
	pic CIRCONVE tics 3
    pic CIRCONVF tics 3
	
texture CIRCONV4
    allowdecals
    pic CIRCONVG tics 3
    pic CIRCONVH tics 3
	pic CIRCONVG tics 3
    pic CIRCONVH tics 3
	
texture CIRCONV5
	allowdecals
    pic CIRCONVI tics 3
    pic CIRCONVJ tics 3
	pic CIRCONVI tics 3
    pic CIRCONVJ tics 3
	
texture CIRBELT
	allowdecals
	pic CIRBELT1 tics 3
	pic CIRBELT2 tics 3
	pic CIRBELT3 tics 3
	pic CIRBELT4 tics 3

texture CIRBELTD
	allowdecals
	pic CIRBELD1 tics 3
	pic CIRBELD2 tics 3
	pic CIRBELD3 tics 3
	pic CIRBELD4 tics 3


////////////////////
///// MMRFPLA /////
flat PLWATX
pic PLWAT1 tics 12
pic PLWAT2 tics 12
pic PLWAT2 tics 12
pic PLWAT4 tics 12

texture PGEFALX
    allowdecals
    pic PGEFAL01 tics 3
	pic PGEFAL02 tics 3
	pic PGEFAL03 tics 3
	
	texture PGETANX
    allowdecals
    pic PGETAN11 tics 5
	pic PGETAN12 tics 5
	pic PGETAN13 tics 5
	pic PGETAN14 tics 5
	
	texture PGEWALM
    allowdecals
    pic PGEWAL05 tics 5
	pic PGEWAL02 tics 5
	pic PGEWAL03 tics 5
	pic PGEWAL04 tics 5

////////////////////
///// MMRFPOW /////
texture POWCON1X
	pic POWCON1A tics 3
	pic POWCON1B tics 3
	
texture POWCON2X
	pic POWCON2A tics 3
	pic POWCON2B tics 3
	
texture POWCON3X
	pic POWCON3A tics 3
	pic POWCON3B tics 3
	
texture POWCON4X
	pic POWCON4A tics 3
	pic POWCON4B tics 3
	
texture POWCON5X
	pic POWCON5A tics 3
	pic POWCON5B tics 3

////////////////////
///// MMRFTHR /////
texture THRISKY
	pic THRISKY1 tics 35
	pic THRISKY2 tics 4


////////////////////
///// MMRFTHE /////
texture BUBBFALX
    allowdecals
    pic BUBBFAL1 tics 3
	pic BUBBFAL2 tics 3
	pic BUBBFAL3 tics 3
	
texture BUBBFA2X
    allowdecals
    pic BUBBFA21 tics 3
	pic BUBBFA22 tics 3
	pic BUBBFA23 tics 3
texture THEFALX
    allowdecals
    pic THELAV1 tics 4
	pic THELAV2 tics 4
	pic THELAV3 tics 4
	
texture THEFA2X
    allowdecals
    pic THELAV4 tics 4
	pic THELAV5 tics 4
	pic THELAV6 tics 4

texture THELAVX
    allowdecals
    pic THELAV7 tics 4
	pic THELAV8 tics 4
	pic THELAV9 tics 4

texture THELA2X
    allowdecals
    pic THELAV10 tics 4
	pic THELAV11 tics 4
	pic THELAV12 tics 4

texture THESKYX
	allowdecals
	pic THESKY1 tics 12
	pic THESKY2 tics 12

////////////////////
///// MMRFPOR /////
texture PORTLEDX
    pic PORTLEDA tics 4
	pic PORTLEDB tics 4
	pic PORTLEDC tics 4
	
cameratexture PORTCAM1 512 512

cameratexture PORTCAM2 512 512

texture PORTLINX
	pic PORTLIN1 tics 4
	pic PORTLIN2 tics 4
	


////////////////////

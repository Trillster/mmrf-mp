actor CircuitBlock
{
	+SOLID
	+SHOOTABLE
	+DONTBLAST
	+NOPAIN
	+THRUSPECIES
	+NOTAUTOAIMED
	+DONTDRAIN
	+CANTSEEK
	+CANPASS
	+USEDAMAGEEVENTSCRIPT
	
	health 50
	Height 64
	Radius 32
	gravity 2.0
	painchance 256
	Scale 2.0
	
	species "MovingPlatform"
	var int user_playerRiding[64];
	States
	{
		Spawn:
			CRPR A 0
			CRPR A 0 ACS_NamedExecuteAlways("core_stickylifts",0,STICKYMODE_GUTSLIFT)
			CRPR A 0 A_ChangeFlag("NOPAIN", false)
		SpawnLoop:
			CRPR A 0 A_JumpIf(CallACS("mmrf_mp_circuitblock_remove"), "DeathWait")
			CRPR A 0 A_SpawnItemEx("CircuitBlockDamager",!(floorz < z) * 16, 0 ,(floorz < z) * -16, momx, momy, momz, 0, SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION, 0)
			CRPR AA 1 A_JumpIfInventory("Once", 1, "Death")
			goto SpawnLoop
		Pain:
			CRPR Z 0 A_PlaySoundEx("misc/metdie", "Voice",0,0)
			CRPR Z 0 A_ChangeFlag("NOPAIN", true)
			CRPR Z 0 A_JumpIf(CallACS("mmrf_mp_circuitblock_remove"), "DeathWait")
			CRPR Z 0 A_SpawnItemEx("CircuitBlockDamager",!(floorz < z) * 16, 0 ,(floorz < z) * -16, momx, momy, momz, 0, SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION, 0)
			CRPR ZZ 1 A_JumpIfInventory("Once", 1, "Death")
			CRPR A 0 A_ChangeFlag("NOPAIN", false)
			goto SpawnLoop
		Death:
			TNT1 A 0 A_PlaySoundEx("misc/mm3explosion", "Weapon")
			TNT1 A 0 A_SpawnItemEx("ExplosionEffect2", 0, 0, 32)
		DeathWait:
			TNT1 A 0 A_ChangeFlag("SOLID", false)
			TNT1 A 2 A_TakeInventory("CutterFlag",999)
			stop
	}
}

actor CircuitBlockDamager
{
	PROJECTILE
	+DONTBLAST
	+DONTREFLECT
	+NOEXPLODEFLOOR
	+THRUSPECIES
	+THRUACTORS
	species "MovingPlatform"
	damagetype "CircuitBlock"
	RenderStyle none
	Damage (20)
	radius 30
	height 62
	
	States
	{
		Spawn:
			CRPR A 0
			CRPR A 0 A_RearrangePointers(AAPTR_TARGET, AAPTR_DEFAULT, AAPTR_TARGET)
			CRPR A 0 A_ClearTarget
			CRPR A 0 A_ChangeFlag("THRUACTORS", false)
			CRPR A 1
			stop
		Crash:
			TNT1 A 0 ACS_NamedExecuteWithResult("mmrf_mp_circuitblock_destroy")
		Death:
			TNT1 A 0
			stop
	}
}

actor HazardTag_CircuitBlock : BasicACSDamager
{
	DamageType "CircuitBlock"
	Obituary "$OB_CIRCUITBLOCK_TAG"
}

actor CircuitBulb_Walls 19558
{
	//$Category MMRF-Props
	//$Title Circuit Bulb (Walls)
	//$Arg0 Damage Sector
	//$Arg1 Default Line Tag
	//$Arg2 Other Line Tag
	+SOLID
	+SHOOTABLE
	+DONTBLAST
	+NOBLOOD
	+NODAMAGE
	+QUICKTORETALIATE
	+DONTRIP
	+NOTAUTOAIMED
	+DONTDRAIN
	+CANTSEEK
	+USEDAMAGEEVENTSCRIPT
	scale 2.0
	height 64
	Radius 16
	Health 100
	painchance 256
	
	States
	{
		Spawn:
			CRPR B 0
			CRPR B 0 ACS_NamedExecuteWithResult("mmrf_mp_circuitbulb_walls", args[0], args[1], args[2])
			goto SpawnOff
		SpawnOff:
			CRPR B 1
		SpawnOn:
			"####" "#" 0 A_JumpIfInventory("CutterFlag",1,"SpawnOff")
			"####" "#" 1 A_JumpIf(true, 1)
			wait
			CRPR C 1
			loop
		Pain:
			"####" "#" 0 A_ChangeFlag(SHOOTABLE, false)
			"####" "#" 1 A_ChangeFlag(NOPAIN, true)
			"####" "#" 0 A_ChangeFlag(NOTARGETSWITCH, true)
			"####" "#" 1 ACS_NamedExecuteWithResult("mmrf_mp_circuitbulb_walls", args[0], args[1], args[2])
			"####" "#" 0 A_ChangeFlag(NOTARGETSWITCH, false)
			"####" "#" 0 A_ClearTarget
			"####" "#" 0 A_ChangeFlag(NOPAIN, false)
			"####" "#" 0 A_ChangeFlag(SHOOTABLE, true)
			Goto SpawnOn
	}
}

actor CircuitBulb_Blocks : CircuitBulb_Walls 19559
{
	//$Title Circuit Bulb (Blocks)
	//$Arg0 Default Map Spot
	//$Arg1 Default Line/Sector Tag
	//$Arg2 Other Map Spot
	//$Arg3 Other Line/Sector Tag
	States
	{
		Spawn:
			CRPR B 0
			CRPR B 0 ACS_NamedExecuteWithResult("mmrf_mp_circuitbulb_blocks", args[0], args[1], args[2], args[3])
			goto SpawnOff
		Pain:
			"####" "#" 0 A_ChangeFlag(SHOOTABLE, false)
			"####" "#" 1 A_ChangeFlag(NOPAIN, true)
			"####" "#" 0 A_ChangeFlag(NOTARGETSWITCH, true)
			"####" "#" 1 ACS_NamedExecuteWithResult("mmrf_mp_circuitbulb_blocks", args[0], args[1], args[2], args[3])
			"####" "#" 0 A_ChangeFlag(NOTARGETSWITCH, false)
			"####" "#" 0 A_ClearTarget
			"####" "#" 0 A_ChangeFlag(NOPAIN, false)
			"####" "#" 0 A_ChangeFlag(SHOOTABLE, true)
			Goto SpawnOn
	}
}

actor CircuitBridge : InvisibleBridge 19832
{
    Radius 16
    Height 32
	species "MovingPlatform"
}

actor CircuitBridgeDamagerSpawner : BasicWatcher
{
	States
	{
		Spawn:
			TNT1 A 0
			TNT1 A 0 A_SpawnItemEx("CircuitBridgeDamager", 0, 0, 0, 0, 0, 0, 0, SXF_NOCHECKPOSITION|SXF_TRANSFERPOINTERS)
			stop
	}
}		

actor CircuitBridgeDamager
{
	PROJECTILE
	+FOILINVUL
	+THRUSPECIES
	+SERVERSIDEONLY
	species "MovingPlatform"
	Damage (0x7FFFFFFD)
	damagetype "CircuitTelefrag"
	height 31
	radius 12
	States
	{
		Spawn:
			TNT1 A 0
			TNT1 A 2
			stop
	}
}

actor CircuitBridgeDamagerCredit : CircuitBridgeDamager
{
	+HITMASTER
	Damage (0)
	States
	{
		XDeath:
			TNT1 A 0 ACS_NamedExecuteWithResult("core_damageowner", 0x7FFFFFFD, AAPTR_MASTER, AAPTR_TRACER)
			stop
	}
}

actor HazardCredit_CircuitBridgeDamagerCredit : BasicACSDamager
{
	+FOILINVUL
	+NODAMAGETHRUST
	DamageType "CircuitTelefrag"
	Obituary "$OB_CIRCUITTELEFRAG_CREDIT"
}

actor HazardTag_CircuitTelefrag : BasicACSDamager
{
	+FOILINVUL
	+NODAMAGETHRUST
	DamageType "CircuitTelefrag"
	Obituary "$OB_CIRCUITTELEFRAG_TAG"
}
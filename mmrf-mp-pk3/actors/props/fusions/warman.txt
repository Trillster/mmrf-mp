actor WarTorchBlue 20983
{
	//$Category MMRF-Props
	+SOLID
	+NOGRAVITY
	+FORCEYBILLBOARD
	height 16
	radius 16
	scale 2.5
	States
	{
		Spawn:
			WMTO ABC 4
			loop 
	}
}

actor WarTorchRed : WarTorchBlue 20984
{
	States
	{
		Spawn:
			WMTO DEF 4
			loop 
	}
}

actor WarStompBlue 19579
{
	//$Category MMRF-Props
	+CLIENTSIDEONLY
	scale 2.5
	States
	{
		Spawn:
			WRPR A 3 A_Jump(4,"Jump")
			loop
		Jump:
			WRPR BCB 2 
			WRPR A 0 ThrustThingZ(0,40,0,0)
			WRPR AAA 2
			WRPR A 1 A_CheckFloor("Land")
			wait
		Land:
			WRPR E 0 A_PlaySoundEX("robot/bigeye", "Voice")
			WRPR BCB 3
			goto Spawn 
	}
}

actor WarStompRed : WarStompBlue 19580
{
	States
	{
		Spawn:
			WRPR D 3 A_Jump(4,"Jump")
			loop
		Jump:
			WRPR EFE 2
			WRPR A 0 ThrustThingZ(0,40,0,0)
			WRPR DDD 2 //the king
			WRPR D 1 A_CheckFloor("Land")
			wait
		Land:
			WRPR E 0 A_PlaySoundEX("robot/bigeye", "Voice")
			WRPR EFE 3
			goto Spawn 
	}
}
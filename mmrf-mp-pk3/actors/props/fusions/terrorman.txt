actor LocoSkull
{
	PROJECTILE
	Renderstyle Translucent
	alpha 0
	Speed 0
	Scale 2.5
	Height 16
	Radius 8
	States
	{
		Spawn:
			LSKL A 0
			LSKL A 0 A_PlaySoundEx("misc/locoskull", "auto")
			LSKL A 35
			LSKL A 0 A_SetTranslucent(1.0)
			LSKL A 0 A_PlaySoundEx("misc/locoskullfly", "auto")
			LSKL A 0 A_ChangeVelocity(40, 0, 0, CVF_RELATIVE|CVF_REPLACE)
			LSKL A 1
			wait
		FadeOut:
			LSKL A 1 A_FadeOut
			wait
	}
}
